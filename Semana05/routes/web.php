<?php

use Illuminate\Support\Facades\Route;
//Agregar la clase del Controlador
use App\Http\Controllers\PhotoController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
// Get vista form
Route::get('/form', function () {
    return view('form');
});
// Agregar nuestro Controlador de recursos
Route::resource('photos', PhotoController::class);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
