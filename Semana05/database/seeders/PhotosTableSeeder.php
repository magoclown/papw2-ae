<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class PhotosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('photos')->insert([
            'name' => Str::random('default'),
            'type' => Str::random('.png'),
        ]);
        DB::table('photos')->insert([
            'name' => Str::random(10),
            'type' => Str::random(3),
        ]);
    }
}
