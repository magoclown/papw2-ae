import { Component, OnInit } from '@angular/core';
import { debug } from 'console';
import { Brewery } from '../models/brewery.model';
import { Card } from '../models/card.model';
import { BreweryService } from '../services/brewery.service';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.less'],
})
export class MainPageComponent implements OnInit {
  public breweries: Card[] = [];
  public breweriesService: Brewery[] = [];
  public imgPath =
    'https://cdna.artstation.com/p/assets/images/images/015/187/072/large/miguel-gallardo-shot1.jpg?1547414328';

  constructor(public breweryService: BreweryService) {}

  ngOnInit(): void {
    this.breweries = this.breweryService.getBreweries();
    this.breweryService.getBreweriesFromService().subscribe((data) => {
      if (data && data !== undefined) {
        this.breweriesService = data;
        console.log(data);
        for (const brewery of this.breweriesService) {
          this.breweries.push(
            new Card(
              this.imgPath,
              brewery.name,
              brewery.street,
              brewery.website_url
            )
          );
        }
      }
    });
  }
}
