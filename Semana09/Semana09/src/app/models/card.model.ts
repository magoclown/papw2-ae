export class Card {
  constructor(
    public imgPath: string,
    public title: string,
    public description: string,
    public labelButton: string
  ) {}
}
