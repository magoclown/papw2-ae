export class Brewery {
  constructor(
    public id: number,
    public name: string,
    public brewery_type: string,
    public street: string,
    public address_2: string,
    public address_3: string,
    public city: string,
    public state: string,
    public county_province: string,
    public postal_code: string,
    public country: string,
    public longitude: string,
    public latitude: string,
    public phone: string,
    public website_url: string,
    public updated_at: string,
    public created_at: string
  ) {}
}
