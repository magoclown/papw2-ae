import { Component, Input, OnInit } from '@angular/core';
import { Card } from 'src/app/models/card.model';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.less']
})
export class CardComponent implements OnInit {

  /**
   * Recibira el modelo para mostrar la informacion
   */
  @Input()
  public cardModel: Card;

  /**
   * Canridad de columnas que abarcara
   */
  @Input()
  public cols: number;

  constructor() { }

  ngOnInit(): void {
  }

  public getClasses(): string {
    return `card`;
  }

}
