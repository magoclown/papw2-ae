import { Component, OnInit } from '@angular/core';
import { Card } from 'src/app/models/card.model';
import { BreweryService } from 'src/app/services/brewery.service';

@Component({
  selector: 'app-new-brewery',
  templateUrl: './new-brewery.component.html',
  styleUrls: ['./new-brewery.component.less'],
})
export class NewBreweryComponent implements OnInit {
  public imgPath: string;
  public title: string;
  public description: string;
  public label: string;

  constructor(private breweryService: BreweryService) {}

  ngOnInit(): void {}

  public onSubmit(e): void {
    // Validacion para evitar que continue
    for (const element of e.srcElement.classList) {
      if (element === 'ng-invalid') {
        return;
      }
    }
    const card = new Card(
      this.imgPath,
      this.title,
      this.description,
      this.label
    );
    this.breweryService.addBrewery(card);
  }
}
