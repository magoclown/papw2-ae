import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewBreweryComponent } from './new-brewery.component';

describe('NewBreweryComponent', () => {
  let component: NewBreweryComponent;
  let fixture: ComponentFixture<NewBreweryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewBreweryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewBreweryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
