import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Brewery } from '../models/brewery.model';
import { Card } from '../models/card.model';

@Injectable({
  providedIn: 'root',
})
export class BreweryService {
  public breweries: Card[] = [];

  constructor(private http: HttpClient) {}

  public getBreweries(): Card[] {
    return this.breweries;
  }

  public addBrewery(brewery: Card): void {
    if (brewery && brewery !== undefined) {
      this.breweries.push(brewery);
    }
  }

  public getBreweriesFromService(): Observable<Brewery[]> {
    return this.http.get<Brewery[]>('https://api.openbrewerydb.org/breweries');
  }
}
