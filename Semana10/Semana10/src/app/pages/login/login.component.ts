import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {

  public id: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router  ) {}

  ngOnInit(): void {
    this.id = this.route.snapshot.queryParams.id;
    const id = this.route.snapshot.paramMap.get('id');
    console.log(this.id);
    console.log(id);
  }

  public onSubmit(e): void {
    e.preventDefault();

    this.router.navigate(['/']);
  }

}
