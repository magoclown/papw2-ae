import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-warning',
  templateUrl: './warning.component.html',
  styleUrls: ['./warning.component.css'],
})
export class WarningComponent implements OnInit {
  @Input()
  messageToShow: string;

  @Output()
  noValid: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor() {}

  ngOnInit(): void {}

  public onHover(): void {
    console.log('On Hover');
    if (this.messageToShow) {
      this.noValid.emit(true);
    }
  }

  public onOut(): void {
    console.log('On Out');
    this.noValid.emit(false);
  }
}
