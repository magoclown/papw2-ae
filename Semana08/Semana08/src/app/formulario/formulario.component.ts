import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css'],
})
export class FormularioComponent implements OnInit {
  public showGreeting: boolean;
  public name: string;
  public now: Date;

  public message: string;

  public inputClass: string;

  constructor() {}

  ngOnInit(): void {}

  public clickButton(): void {
    this.message = null;
    console.log(this.name);
    this.showGreeting = true;
    this.now = new Date();
    if (!this.name || this.name === undefined) {
      this.message = 'Nombre no definidio, ingrese un valor valido';
    }

  }

  public changeInput(e): void {
    // console.log(e);
    if (this.name.length <= 0) {
      this.showGreeting = false;
    }
  }

  public noValid(e): void {
    console.log(e);
    this.inputClass = (e) ? 'input-no-valid' : 'input-valid';
  }
}
