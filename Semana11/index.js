const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const app = express();
const port = 3000;

const myLogger = (req,res,next) => {
    console.log('Logged');
    next();
};

app.use(express.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());
app.use(myLogger);

app.get("/", (req, res) => {
  res.send("Hello World!");
});

app.get("/bye", (req, res) => {
  res.send("Bye World!");
});

app.get("/json", (req, res) => {
  res.json({ name: "Jose", age: 26 });
});

app.get("/query", (req, res) => {
  // http://localhost:3000/query?name=Jose&k=playstation
  console.log(req.query);
  res.json(req);
});

app.get("/params/:id", (req, res) => {
  // http://localhost:3000/params/20
  console.log(req.params.id);
  res.send(`El ID fue ${req.params.id}`);
});

app.post("/body", (req, res) => {
  // http://localhost:3000/body
  console.log(req);
  res.send(`Recibido ${req.body}`);
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
