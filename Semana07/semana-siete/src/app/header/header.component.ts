import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  title = 'Semana 07';

  dropdownList = ['Limones', 'Peras', 'Manzanas', 'Sandias'];

  constructor() {}

  ngOnInit(): void {}
}
