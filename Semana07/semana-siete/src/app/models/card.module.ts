export class Card {
  constructor(
    public url: string,
    public title: string,
    public description: string
  ) {}
}
