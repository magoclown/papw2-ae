import { Component, OnInit } from '@angular/core';
import { Card } from './models/card.module';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'Semana 7';
  subtitle = 'Esta Funcionando!';

  carroulseImages = [
    'https://cdnb.artstation.com/p/assets/images/images/029/959/143/4k/dao-trong-le-yordle-glade-farmer-3-1.jpg?1599150301',
    'https://cdna.artstation.com/p/assets/images/images/030/008/722/4k/dao-trong-le-spacey-sketcher.jpg?1599315124',
    'https://cdna.artstation.com/p/assets/images/images/030/008/722/4k/dao-trong-le-spacey-sketcher.jpg?1599315124',
  ];

  public card: Card;

  ngOnInit(): void {
    this.card = new Card(
      'https://cdna.artstation.com/p/assets/images/images/030/298/218/large/cesar-zambelli-veio-01.jpg?1600186794',
      'Dibujo',
      'Dibujo de persona de mayor edad'
    );
  }
}
