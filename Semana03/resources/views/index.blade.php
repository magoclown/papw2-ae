@extends('layouts.base')

@section('title', 'Welcome')

@section('news-title', 'Se da la primera clase de Blades')

@section('news-content')
Dessert cotton candy pudding sesame snaps. Jujubes tiramisu caramels. Toffee jelly-o jelly tootsie roll jelly-o gingerbread dragée.

Sweet chocolate cake brownie toffee croissant sweet roll. Jelly-o tiramisu candy canes ice cream muffin. Chocolate cake cotton candy candy canes pie.

Tootsie roll cotton candy chocolate bar tart pie bonbon cheesecake. Muffin cake marzipan dragée bear claw dragée cake. Chocolate cake sweet roll halvah halvah.

Sugar plum candy apple pie oat cake cookie pastry apple pie. Sesame snaps gummi bears pudding chocolate cake dessert gummies cupcake sweet. Wafer bonbon lemon drops candy canes cupcake cake.

Cheesecake jelly-o gummies caramels donut halvah. Jujubes tootsie roll candy lollipop carrot cake cake tiramisu marzipan. Brownie chocolate icing.
@endsection
