@extends('layouts.app')

@section('title', 'Page Title')

@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@endsection

@section('content')
    <p>Hello, {{ $name }}.</p>
    <p>This is my body content.</p>
    @component('components.alert')
        @slot('slotName')
            Orihuela
        @endslot
    @endcomponent
@endsection

@if (true)
    <p>True</p>
@else
    <p>False</p>
@endif
