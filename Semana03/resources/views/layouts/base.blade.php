<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <script src="{{ asset('js/app.js')}}"></script>
    <title>@yield('title')</title>
</head>
<body>
    <header>
        <h1>PAGINA DE NOTICIAS BIEN CHIDA</h1>
    </header>
    <section>
        <article>
            <h2>@yield('news-title')</h2>
            <p>
                @yield('news-content')
            </p>
        </article>
    </section>
    <footer>

    </footer>
</body>
</html>
